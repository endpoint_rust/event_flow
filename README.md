# event_flow

#### 介绍
这是一个事件编排引擎，目标是更通用的事件接入能力、更复杂可配置的事件处理能力、支持常规以及高级安全场景和多种sink方式，是一种低代码平台。

借鉴了很多开源软件的思路：Polars、arrow-datafusion、Vector、tremor、fluvio、datafuse、dynparser、chumsky等

#### 软件架构

数据从各种Source接入，以WorkSpace进行隔离（业务逻辑、系统资源），

WorkSpace内部以WorkNode网格的方式对数据进行处理，数据在各WorkNode之间以写时拷贝的方式进行共享。

目前对数据的各种处理，是通过灵活可嵌套的自定义DML来进行的。

#### 自定义DML描述

目前自定义DML大体上分4类：filter、hand、window、aggregation

    具体的列表以及参数、功能描述，后续空了来补齐。

        

#### 功能扩充

1. 参考fluvio的思路，利用WebAssembly进行更灵活的自定义功能扩充。

    利用wasm进行扩充总体思路描述：wasm runtime暂选官方wasmtime，支持其他语言编译成wasm32-wasi target.

    [我对WebAssembly之wasmtime的资料汇总以及测试学习](https://gitee.com/wasmtime)

    再灵活的DML也不能满足所有自定义情况，所以成熟的数据处理平台一定要支持功能扩充。传统的功能扩充无非就几种方式：加载动态库、SDK、DML等，现在我们有了新的选择。

    建议使用AssemblyScript进行wasm target的编写，用灵活的脚本语言就能写出近似原生性能的功能扩充！！！

    [我对AssemblyScript的资料汇总以及测试学习](https://gitee.com/wasmtime/assembly-script)

    1.1 第一步先进行filter方面的扩充

    2.2 对单个类型字段进行自定义处理

2. 安全工具箱方面的扩充

    2.1 支持热加载并通过DML进行弱口令方面的功能调用

    2.2 支持热加载并通过DML进行资产识别方面的功能调用

    2.3 支持热加载并通过DML进行应用识别方面的功能调用

    2.4 支持加载各种风格的安全规则
   



