
#[derive(Debug,Clone)]
pub enum Ast {
    Value(String),
    Rule((String, Vec<Ast>))
}