use std::collections::HashMap;
use crate::conf_parse::expression::Expression;

#[derive(Clone, Debug)]
pub struct Rules{
    pub rules: HashMap<String, Expression>,
}

impl Rules{
    pub fn new()->Rules{
        return Rules{
            rules: HashMap::new(),
        };
    }
    pub fn add(mut self, name: &str, expression: Expression)->Self{
        self.rules.insert(name.to_string(), expression);
        return self;
    }
    pub fn merge(self, other: Self)->Self{
        Rules{rules: other.rules.into_iter().chain(self.rules).collect()}
    }
    pub fn get(&self, name: &str)->Option<&Expression>{
        return self.rules.get(name);
    }
}