use std::sync::Arc;
use crate::data_frame::{DataFrame, MetaData};
use crate::physical_plan::window::SortByValue;
use crate::run_plan::HandResult;
use crate::data_frame::value::{ElementValue, RealValue};
use std::fmt::Debug;

pub struct SingleData{
    pub data: Arc<dyn DataFrame>, //data要在多个work node之间共享，所以不支持修改，修改记录放在metadata里面。
    pub metadata: MetaData,
}

impl Clone for SingleData{
    fn clone(&self) -> Self{
        self.mild_clone()
    }
}

impl SingleData{
    pub fn new(d: Arc<dyn DataFrame>)->Self{
        SingleData{
            data: d,
            metadata: MetaData::new(),
        }
    }
    pub fn deep_clone(&self)->Self{
        Self{
            data: Arc::clone(&self.data),
            metadata: self.metadata.clone(),
        }
    }
    /*
        不继承Metadata，一般用于一些需要对SingleData clone处理的情况，
        同时新clone的数据又不会被继续继承处理。
    */
    pub fn mild_clone(&self)->Self{
        Self{
            data: Arc::clone(&self.data),
            metadata: MetaData::new(),
        }
    }
}

impl Debug for SingleData{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SingleData")
            .finish()
    }
}

impl SortByValue<ElementValue, RealValue> for SingleData{
    fn get_key_value_always(&self, k: &ElementValue) ->RealValue{
        match self.data.get_key_value(k){
            HandResult::Ok(t) => {t},
            HandResult::Err(()) => {RealValue::None},
        }
    }
}
