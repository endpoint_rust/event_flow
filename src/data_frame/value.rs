use crate::run_plan::HandResult;
use chrono::{DateTime, ParseResult};
use crate::physical_plan::PhysicalPlanExprArg;
use std::cmp::Ordering;
use chrono_tz::Tz;
use chrono::Duration as ChronoDuration;
use std::hash::Hash;
use std::hash::Hasher;
use std::str::FromStr;
use std::fmt;
use chrono::TimeZone;
use crate::conf_init::{MapResult, MapErr};

//配置也页面值，不指定值的类型，以期更通用
#[derive(Debug,Clone,Hash)]
pub struct ConfValue{
    pub value: String,
    compare_operation: Option<CompareOperation>,
    rv_u8: Option<RealValue>,
    rv_u16: Option<RealValue>,
    rv_u32: Option<RealValue>,
    rv_u64: Option<RealValue>,
    rv_boolean: Option<RealValue>,
    rv_datetime: Option<RealValue>,
    rv_null: Option<RealValue>,
    rv_utf8: Option<RealValue>,
    rv_i8: Option<RealValue>,
    rv_i16: Option<RealValue>,
    rv_i32: Option<RealValue>,
    rv_i64: Option<RealValue>,
    rv_f32: Option<RealValue>,
    rv_f64: Option<RealValue>,
    rv_range: Option<RealValue>,
    rv_durataion: Option<RealValue>,
    rv_bytes: Option<RealValue>,
}

//有具体类型的值，需要严格值类型匹配
#[derive(Clone,Debug)]
pub enum RealValue{
    None,            //用于某些get情况下的不存在或者某些执行函数不需要返回值
    Boolean(bool),   //bool_false
    Utf8(String),    //utf8_aaaa
    U8(u8),          //u8_32
    U16(u16),        //u16_32
    U32(u32),        //u32_32
    U64(u64),        //u64_32
    I8(i8),          //i8_43
    I16(i16),        //i16_43
    I32(i32),        //i32_43
    I64(i64),        //i64_43
    F32(f32),        //f32_43.2
    F64(f64),        //f64_43.2
    Bytes(Vec<u8>),  //bytes_0a0c32
    Range(u64,u64),
    DateTime(DateTime<Tz>),//time_2009-06-15 13:45:30
    Duration(ChronoDuration),//duration_322             多少微秒
}

//另外一个元素的值
#[derive(Clone,Debug,Eq,PartialEq,Hash)]
pub struct ElementValue{
    pub element: Vec<String>,
}

pub enum RealValueType{
    None,
    Boolean,
    Utf8,
    U8,
    U16,
    U32,
    U64,
    I8,
    I16,
    I32,
    I64,
    F32,
    F64,
    Bytes,
    Range,
    DateTime,
    Duration,
}

pub enum ElementRealValue{
    ElementValue(ElementValue),
    RealValue(RealValue),
}

#[derive(Debug,Clone)]
pub enum ElementConfValue{
    ElementValue(ElementValue),
    ConfValue(ConfValue),
}

#[derive(Debug,Clone)]
pub enum ElementRealConfValue{
    ElementValue(ElementValue),
    RealValue(RealValue),
    ConfValue(ConfValue),
}
#[derive(Clone,Debug)]
pub enum RealConfValue{
    RealValue(RealValue),
    ConfValue(ConfValue),
}

#[derive(Debug,Clone,Hash)]
pub enum CompareOperation {
    Gt,
    Ge,
    Lt,
    Le,
    Eq,
    Neq,
}

impl ConfValue{
    fn _to_compare_operation(&mut self) {
        match self.value.as_str() {
            "gt" => { self.compare_operation = Some(CompareOperation::Gt); },
            "ge" => { self.compare_operation = Some(CompareOperation::Ge); },
            "lt" => { self.compare_operation = Some(CompareOperation::Lt); },
            "le" => { self.compare_operation = Some(CompareOperation::Le); },
            "eq" => { self.compare_operation = Some(CompareOperation::Eq); },
            "neq" => { self.compare_operation = Some(CompareOperation::Neq); },
            _ => { self.compare_operation = None; },
        }
    }
    fn _to_u8(&mut self){
        if let Ok(t) = self.value.parse::<u8>(){
            self.rv_u8 = Some(RealValue::U8(t));
        }
    }
    fn _to_u16(&mut self){
        if let Ok(t) = self.value.parse::<u16>(){
            self.rv_u16 = Some(RealValue::U16(t));
        }
    }
    fn _to_u32(&mut self){
        if let Ok(t) = self.value.parse::<u32>(){
            self.rv_u32 = Some(RealValue::U32(t));
        }
    }
    fn _to_u64(&mut self){
        if let Ok(t) = self.value.parse::<u64>(){
            self.rv_u64 = Some(RealValue::U64(t));
        }
    }
    fn _to_i8(&mut self){
        if let Ok(t) = self.value.parse::<i8>(){
            self.rv_i8 = Some(RealValue::I8(t));
        }
    }
    fn _to_i16(&mut self){
        if let Ok(t) = self.value.parse::<i16>(){
            self.rv_i16 = Some(RealValue::I16(t));
        }
    }
    fn _to_i32(&mut self){
        if let Ok(t) = self.value.parse::<i32>(){
            self.rv_i32 = Some(RealValue::I32(t));
        }
    }
    fn _to_i64(&mut self){
        if let Ok(t) = self.value.parse::<i64>(){
            self.rv_i64 = Some(RealValue::I64(t));
        }
    }
    fn _to_f32(&mut self){
        if let Ok(t) = self.value.parse::<f32>(){
            self.rv_f32 = Some(RealValue::F32(t));
        }
    }
    fn _to_f64(&mut self){
        if let Ok(t) = self.value.parse::<f64>(){
            self.rv_f64 = Some(RealValue::F64(t));
        }
    }
    fn _to_boolean(&mut self){
        if self.value == "yes" ||
            self.value == "YES" ||
            self.value == "ok" ||
            self.value == "OK" ||
            self.value == "true" ||
            self.value == "TRUE"{
            self.rv_boolean = Some(RealValue::Boolean(true));
        }else if self.value == "no" ||
            self.value == "NO" ||
            self.value == "fail" ||
            self.value == "FAIL" ||
            self.value == "FALSE"{
            self.rv_boolean = Some(RealValue::Boolean(false));
        }
    }

    pub fn new(value: String)->ConfValue{
        let mut tmp = ConfValue{
            value: value,
            compare_operation: None,
            rv_u8: None,
            rv_u16: None,
            rv_u32: None,
            rv_u64: None,
            rv_boolean: None,
            rv_datetime: None,
            rv_null: None,
            rv_utf8: None,
            rv_i8: None,
            rv_i16: None,
            rv_i32: None,
            rv_i64: None,
            rv_f32: None,
            rv_f64: None,
            rv_range: None,
            rv_durataion: None,
            rv_bytes: None,
        };
        tmp._to_compare_operation();
        tmp._to_u8();
        tmp._to_u16();
        tmp._to_u32();
        tmp._to_u64();
        tmp._to_i8();
        tmp._to_i16();
        tmp._to_i32();
        tmp._to_i64();
        tmp._to_f32();
        tmp._to_f64();
        tmp._to_boolean();
        tmp
    }

    pub fn compare_operation(&self)->MapResult<CompareOperation>{
        if let Some(co) = &self.compare_operation{
            return Ok(co.clone());
        }else{
            return MapResult::Err(MapErr::new(format!("only accept [gt/ge/lt/le/eq/neq]")));
        }
    }
    pub fn to_datetime(&self)-> HandResult {
        if let Some(ref t) = self.rv_datetime{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_bytes(&self)-> HandResult {
        if let Some(ref t) = self.rv_bytes{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_u8(&self)-> HandResult {
        if let Some(ref t) = self.rv_u8{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_u16(&self)-> HandResult {
        if let Some(ref t) = self.rv_u16{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_u32(&self)-> HandResult {
        if let Some(ref t) = self.rv_u32{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_u64(&self)-> HandResult {
        if let Some(ref t) = self.rv_u64{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn try_u64(&self)->Result<u64,()> {
        if let Some(ref t) = self.rv_u64 {
            if let RealValue::U64(t) = t {
                return Ok(*t);
            }
        }
        return Err(());
    }
    pub fn to_i8(&self)-> HandResult {
        if let Some(ref t) = self.rv_i8{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_i16(&self)-> HandResult {
        if let Some(ref t) = self.rv_i16{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_i32(&self)-> HandResult {
        if let Some(ref t) = self.rv_i32{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_i64(&self)-> HandResult {
        if let Some(ref t) = self.rv_i64{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_f32(&self)-> HandResult {
        if let Some(ref t) = self.rv_f32{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_f64(&self)-> HandResult {
        if let Some(ref t) = self.rv_f64{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_boolean(&self)-> HandResult {
        if let Some(ref t) = self.rv_boolean{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_null(&self) -> HandResult {
        if let Some(ref t) = self.rv_null{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_utf8(&self) -> HandResult {
        if let Some(ref t) = self.rv_utf8{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_range(&self) -> HandResult {
        if let Some(ref t) = self.rv_range{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }
    pub fn to_duration(&self) -> HandResult {
        if let Some(ref t) = self.rv_durataion{
            return HandResult::Ok(t.clone());
        }else{
            return HandResult::Err(());
        }
    }

    pub fn lt(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.lt(right);
    }
    pub fn _lt(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.lt(&right.change(left)?);
    }
    pub fn le(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.le(right);
    }
    pub fn _le(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.le(&right.change(left)?);
    }
    pub fn gt(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.gt(right);
    }
    pub fn _gt(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.gt(&right.change(left)?);
    }
    pub fn ge(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.ge(right);
    }
    pub fn _ge(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.ge(&right.change(left)?);
    }
    pub fn eq(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.eq(right);
    }
    pub fn _eq(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.eq(&right.change(left)?);
    }
    pub fn neq(&self, right: &RealValue)-> HandResult {
        return self.change(right)?.neq(right);
    }
    pub fn _neq(left: &RealValue, right: &ConfValue)-> HandResult {
        return left.neq(&right.change(left)?);
    }
    fn change(&self, other: &RealValue)-> HandResult {
        let left = match other{
            RealValue::U8(_) => { self.to_u8()? },
            RealValue::U16(_) => { self.to_u16()? },
            RealValue::U32(_) => { self.to_u32()? },
            RealValue::U64(_) => { self.to_u64()? },
            RealValue::I8(_) => { self.to_i8()? },
            RealValue::I16(_) => { self.to_i16()? },
            RealValue::I32(_) => { self.to_i32()? },
            RealValue::I64(_) => { self.to_i64()? },
            RealValue::F32(_) => { self.to_f32()? },
            RealValue::F64(_) => { self.to_f64()? },
            RealValue::None => { self.to_null()? },
            RealValue::Boolean(_) => { self.to_boolean()? },
            RealValue::Duration(_) => { self.to_duration()? },
            RealValue::Utf8(_) => { self.to_utf8()? },
            RealValue::Range(_,_) => { self.to_range()? },
            RealValue::DateTime(_) => { self.to_datetime()? }
            RealValue::Bytes(_) => { self.to_bytes()? },
        };
        return HandResult::Ok(left);
    }
}

impl RealValueType{
    pub fn from_str(desc: &str)->Option<RealValueType>{
        match desc {
            "none" => return Some(RealValueType::None),
            "bool" => return Some(RealValueType::Boolean),
            "str" => return Some(RealValueType::Utf8),
            "u8" => return Some(RealValueType::U8),
            "u16" => return Some(RealValueType::U16),
            "u32" => return Some(RealValueType::U32),
            "u64" => return Some(RealValueType::U64),
            "i8" => return Some(RealValueType::I8),
            "i16" => return Some(RealValueType::I16),
            "i32" => return Some(RealValueType::I32),
            "i64" => return Some(RealValueType::I64),
            "f32" => return Some(RealValueType::F32),
            "f64" => return Some(RealValueType::F64),
            "bytes" => return Some(RealValueType::Bytes),
            "range" => return Some(RealValueType::Range),
            "datetime" => return Some(RealValueType::DateTime),
            "duration" => return Some(RealValueType::Duration),
            _ => return None,
        }
    }
}

/*
    用于对一组数据按元素进行排序
*/
impl PartialEq for RealValue{
    fn eq(&self, right: &Self) -> bool{
        match self{
            RealValue::None => {
                match &right{
                    RealValue::None => {true},
                    _ => {false},
                }
            },
            RealValue::Boolean(left) => {
                match &right{
                    RealValue::Boolean(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::Utf8(left) => {
                match &right{
                    RealValue::Utf8(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::U8(left) => {
                match &right{
                    RealValue::U8(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::U16(left) => {
                match &right{
                    RealValue::U16(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::U32(left) => {
                match &right{
                    RealValue::U32(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::U64(left) => {
                match &right{
                    RealValue::U64(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::I8(left) => {
                match &right{
                    RealValue::I8(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::I16(left) => {
                match &right{
                    RealValue::I16(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::I32(left) => {
                match &right{
                    RealValue::I32(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::I64(left) => {
                match &right{
                    RealValue::I64(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::F32(left) => {
                match &right{
                    RealValue::F32(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::F64(left) => {
                match &right{
                    RealValue::F64(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::Bytes(left) => {
                match &right{
                    RealValue::Bytes(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::Range(ll,lr) => {
                match &right{
                    RealValue::Range(rl,rr) => {
                        ll.eq(rl) && lr.eq(rr)
                    },
                    _ => {false},
                }
            },
            RealValue::DateTime(left) => {
                match &right{
                    RealValue::DateTime(right) => {left.eq(right)},
                    _ => {false},
                }
            },
            RealValue::Duration(left) => {
                match &right{
                    RealValue::Duration(right) => {left.eq(right)},
                    _ => {false},
                }
            }
        }
    }
}
impl PartialOrd for RealValue{
    fn partial_cmp(&self, right: &Self) -> Option<Ordering>{
        match self{
            RealValue::None => {None},
            RealValue::Boolean(_) => {None},
            RealValue::Utf8(left) => {
                match &right{
                    RealValue::Utf8(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::U8(left) => {
                match &right{
                    RealValue::U8(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::U16(left) => {
                match &right{
                    RealValue::U16(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::U32(left) => {
                match &right{
                    RealValue::U32(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::U64(left) => {
                match &right{
                    RealValue::U64(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::I8(left) => {
                match &right{
                    RealValue::I8(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::I16(left) => {
                match &right{
                    RealValue::I16(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::I32(left) => {
                match &right{
                    RealValue::I32(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::I64(left) => {
                match &right{
                    RealValue::I64(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::F32(left) => {
                match &right{
                    RealValue::F32(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::F64(left) => {
                match &right{
                    RealValue::F64(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::Bytes(left) => {
                match &right{
                    RealValue::Bytes(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::Range(_,_) => {None},
            RealValue::DateTime(left) => {
                match &right{
                    RealValue::DateTime(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            },
            RealValue::Duration(left) => {
                match &right{
                    RealValue::Duration(right) => {left.partial_cmp(right)},
                    _ => {None},
                }
            }
        }
    }
}

impl RealValue{
    pub fn from_conf_str(desc: &str)->Option<RealValue>{
        if let Some(bool) = desc.strip_prefix("bool_"){
            if let Ok(bool) = bool::from_str(bool){
                return Some(RealValue::Boolean(bool));
            }else{
                return None;
            }
        }else if let Some(utf8) = desc.strip_prefix("utf8_"){
            return Some(RealValue::Utf8(utf8.to_string()));
        }else if let Some(u8) = desc.strip_prefix("u8_"){
            if let Ok(u8) = u8::from_str(u8){
                return Some(RealValue::U8(u8));
            }else{
                return None;
            }
        }else if let Some(u16) = desc.strip_prefix("u16_"){
            if let Ok(u16) = u16::from_str(u16){
                return Some(RealValue::U16(u16));
            }else{
                return None;
            }
        }else if let Some(u32) = desc.strip_prefix("u32_"){
            if let Ok(u32) = u32::from_str(u32){
                return Some(RealValue::U32(u32));
            }else{
                return None;
            }
        }else if let Some(u64) = desc.strip_prefix("u64_"){
            if let Ok(u64) = u64::from_str(u64){
                return Some(RealValue::U64(u64));
            }else{
                return None;
            }
        }else if let Some(i8) = desc.strip_prefix("i8_"){
            if let Ok(i8) = i8::from_str(i8){
                return Some(RealValue::I8(i8));
            }else{
                return None;
            }
        }else if let Some(i16) = desc.strip_prefix("i16_"){
            if let Ok(i16) = i16::from_str(i16){
                return Some(RealValue::I16(i16));
            }else{
                return None;
            }
        }else if let Some(i32) = desc.strip_prefix("i32_"){
            if let Ok(i32) = i32::from_str(i32){
                return Some(RealValue::I32(i32));
            }else{
                return None;
            }
        }else if let Some(i64) = desc.strip_prefix("i64_"){
            if let Ok(i64) = i64::from_str(i64){
                return Some(RealValue::I64(i64));
            }else{
                return None;
            }
        }else if let Some(f32) = desc.strip_prefix("f32_"){
            if let Ok(f32) = f32::from_str(f32){
                return Some(RealValue::F32(f32));
            }else{
                return None;
            }
        }else if let Some(f64) = desc.strip_prefix("f64_"){
            if let Ok(f64) = f64::from_str(f64){
                return Some(RealValue::F64(f64));
            }else{
                return None;
            }
        }else if let Some(time) = desc.strip_prefix("time_"){
            let tz = Tz::Asia__Shanghai;
            if let ParseResult::Ok(time_tz) = tz.datetime_from_str(time, "%Y-%m-%d %H:%M:%S"){
                return Some(RealValue::DateTime(time_tz));
            }else{
                return None;
            }
        }else{
            return None;
        }
    }

    pub fn to_string(&self) ->String{
        match self{
            RealValue::None => {String::new()},
            RealValue::Boolean(v) => {format!("{}", v)},
            RealValue::Utf8(v) => {v.clone()},
            RealValue::U8(v) => {format!("{}", v)},
            RealValue::U16(v) => {format!("{}", v)},
            RealValue::U32(v) => {format!("{}", v)},
            RealValue::U64(v) => {format!("{}", v)},
            RealValue::I8(v) => {format!("{}", v)},
            RealValue::I16(v) => {format!("{}", v)},
            RealValue::I32(v) => {format!("{}", v)},
            RealValue::I64(v) => {format!("{}", v)},
            RealValue::F32(v) => {format!("{}", v)},
            RealValue::F64(v) => {format!("{}", v)},
            RealValue::Range(v1,v2) => {format!("{}-{}",v1,v2)},
            RealValue::Bytes(v) => {format!("{:?}", v)},
            RealValue::DateTime(v) => {format!("{:?}", v)},
            RealValue::Duration(v) => {format!("{:?}", v)},
        }
    }

    pub fn convert(&self, new_type: String) ->RealValue {
        match RealValueType::from_str(&new_type) {
            Some(t) => {
                match t {
                    RealValueType::Boolean => {
                        match self {
                            RealValue::Utf8(s) => {
                                match s.to_lowercase().as_str() {
                                    "true" | "t" | "yes" | "y" => return RealValue::Boolean(true),
                                    "false" | "f" | "no" | "n" | "0" => return RealValue::Boolean(false),
                                    _ => return RealValue::None,
                                }
                            }
                            RealValue::I8(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::I16(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::I32(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::I64(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::U8(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::U16(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::U32(n) => {return RealValue::Boolean(*n!=0)}
                            RealValue::U64(n) => {return RealValue::Boolean(*n!=0)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::Utf8 => {RealValue::Utf8(self.to_string())}
                    RealValueType::U8 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::U8(*n as u8)}
                            RealValue::I16(n) => {return RealValue::U8(*n as u8)}
                            RealValue::I32(n) => {return RealValue::U8(*n as u8)}
                            RealValue::I64(n) => {return RealValue::U8(*n as u8)}
                            RealValue::U8(n) => {return RealValue::U8(*n as u8)}
                            RealValue::U16(n) => {return RealValue::U8(*n as u8)}
                            RealValue::U32(n) => {return RealValue::U8(*n as u8)}
                            RealValue::U64(n) => {return RealValue::U8(*n as u8)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::U16 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::U16(*n as u16)}
                            RealValue::I16(n) => {return RealValue::U16(*n as u16)}
                            RealValue::I32(n) => {return RealValue::U16(*n as u16)}
                            RealValue::I64(n) => {return RealValue::U16(*n as u16)}
                            RealValue::U8(n) => {return RealValue::U16(*n as u16)}
                            RealValue::U16(n) => {return RealValue::U16(*n as u16)}
                            RealValue::U32(n) => {return RealValue::U16(*n as u16)}
                            RealValue::U64(n) => {return RealValue::U16(*n as u16)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::U32 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::U32(*n as u32)}
                            RealValue::I16(n) => {return RealValue::U32(*n as u32)}
                            RealValue::I32(n) => {return RealValue::U32(*n as u32)}
                            RealValue::I64(n) => {return RealValue::U32(*n as u32)}
                            RealValue::U8(n) => {return RealValue::U32(*n as u32)}
                            RealValue::U16(n) => {return RealValue::U32(*n as u32)}
                            RealValue::U32(n) => {return RealValue::U32(*n as u32)}
                            RealValue::U64(n) => {return RealValue::U32(*n as u32)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::U64 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::U64(*n as u64)}
                            RealValue::I16(n) => {return RealValue::U64(*n as u64)}
                            RealValue::I32(n) => {return RealValue::U64(*n as u64)}
                            RealValue::I64(n) => {return RealValue::U64(*n as u64)}
                            RealValue::U8(n) => {return RealValue::U64(*n as u64)}
                            RealValue::U16(n) => {return RealValue::U64(*n as u64)}
                            RealValue::U32(n) => {return RealValue::U64(*n as u64)}
                            RealValue::U64(n) => {return RealValue::U64(*n as u64)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::I8 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::I8(*n as i8)}
                            RealValue::I16(n) => {return RealValue::I8(*n as i8)}
                            RealValue::I32(n) => {return RealValue::I8(*n as i8)}
                            RealValue::I64(n) => {return RealValue::I8(*n as i8)}
                            RealValue::U8(n) => {return RealValue::I8(*n as i8)}
                            RealValue::U16(n) => {return RealValue::I8(*n as i8)}
                            RealValue::U32(n) => {return RealValue::I8(*n as i8)}
                            RealValue::U64(n) => {return RealValue::I8(*n as i8)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::I16 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::I16(*n as i16)}
                            RealValue::I16(n) => {return RealValue::I16(*n as i16)}
                            RealValue::I32(n) => {return RealValue::I16(*n as i16)}
                            RealValue::I64(n) => {return RealValue::I16(*n as i16)}
                            RealValue::U8(n) => {return RealValue::I16(*n as i16)}
                            RealValue::U16(n) => {return RealValue::I16(*n as i16)}
                            RealValue::U32(n) => {return RealValue::I16(*n as i16)}
                            RealValue::U64(n) => {return RealValue::I16(*n as i16)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::I32 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::I32(*n as i32)}
                            RealValue::I16(n) => {return RealValue::I32(*n as i32)}
                            RealValue::I32(n) => {return RealValue::I32(*n as i32)}
                            RealValue::I64(n) => {return RealValue::I32(*n as i32)}
                            RealValue::U8(n) => {return RealValue::I32(*n as i32)}
                            RealValue::U16(n) => {return RealValue::I32(*n as i32)}
                            RealValue::U32(n) => {return RealValue::I32(*n as i32)}
                            RealValue::U64(n) => {return RealValue::I32(*n as i32)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::I64 => {
                        match self {
                            RealValue::I8(n) => {return RealValue::I64(*n as i64)}
                            RealValue::I16(n) => {return RealValue::I64(*n as i64)}
                            RealValue::I32(n) => {return RealValue::I64(*n as i64)}
                            RealValue::I64(n) => {return RealValue::I64(*n as i64)}
                            RealValue::U8(n) => {return RealValue::I64(*n as i64)}
                            RealValue::U16(n) => {return RealValue::I64(*n as i64)}
                            RealValue::U32(n) => {return RealValue::I64(*n as i64)}
                            RealValue::U64(n) => {return RealValue::I64(*n as i64)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::F32 => {
                        match self {
                            RealValue::F32(n) => {return RealValue::F32(*n as f32)}
                            RealValue::F64(n) => {return RealValue::F32(*n as f32)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::F64 => {
                        match self {
                            RealValue::F32(n) => {return RealValue::F64(*n as f64)}
                            RealValue::F64(n) => {return RealValue::F64(*n as f64)}
                            _ => return RealValue::None
                        }
                    }
                    RealValueType::DateTime => {
                        let tz = Tz::Asia__Shanghai;
                        match self {
                            RealValue::I64(t) => {RealValue::DateTime(tz.timestamp(*t, 0))}
                            RealValue::U64(t) => {RealValue::DateTime(tz.timestamp(*t as i64, 0))}
                            _ => RealValue::None,
                        }
                    }
                    _ => {return RealValue::None} //Bytes Range Duration
                }
            }
            None => {return RealValue::None}
        }
    }

    pub fn to_physical_expr(self)->Box<PhysicalPlanExprArg>{
        Box::new(PhysicalPlanExprArg::RealValue(self))
    }

    pub fn to_conf_value(&self)->ConfValue{
        let tmp = self.to_string();
        ConfValue::new(tmp)
    }
    /*
        这里是尽量转换的原则，所以如果有特殊需求，可以通过filter或者condition来规避
    */
    pub fn to_usize(&self)->usize{
        match self{
            RealValue::None => {0},
            RealValue::Boolean(v) => {if *v{1}else{0}},
            RealValue::Utf8(v) => {
                if let Ok(v) = usize::from_str(v.as_str()){
                    v
                }else{
                    0
                }
            },
            RealValue::U8(v) => {*v as usize},
            RealValue::U16(v) => {*v as usize},
            RealValue::U32(v) => {*v as usize},
            RealValue::U64(v) => {*v as usize},
            RealValue::I8(v) => {*v as usize},
            RealValue::I16(v) => {*v as usize},
            RealValue::I32(v) => {*v as usize},
            RealValue::I64(v) => {*v as usize},
            RealValue::F32(v) => {*v as usize},
            RealValue::F64(v) => {*v as usize},
            RealValue::Range(v1,v2) => {0},
            RealValue::Bytes(v) => {0},
            RealValue::DateTime(v) => {0},
            RealValue::Duration(v) => {0},
        }
    }
}

impl Hash for RealValue{
    fn hash<H: Hasher>(&self, state: &mut H){
        match self{
            RealValue::Utf8(t) => {t.hash(state);},
            RealValue::U8(t) => {t.hash(state);},
            RealValue::U16(t) => {t.hash(state);},
            RealValue::U32(t) => {t.hash(state);},
            RealValue::U64(t) => {t.hash(state);},
            RealValue::I8(t) => {t.hash(state);},
            RealValue::I16(t) => {t.hash(state);},
            RealValue::I32(t) => {t.hash(state);},
            RealValue::I64(t) => {t.hash(state);},
            RealValue::DateTime(_t) => {},
            RealValue::Duration(t) => {t.hash(state);},
            RealValue::Boolean(t) => {t.hash(state);},
            RealValue::Range(left,right) => {left.hash(state);right.hash(state);},
            _ => {},
        }
    }
}

impl fmt::Display for RealValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::None => write!(f, "null"),
            Self::Boolean(b) => write!(f, "{}", b),
            Self::Utf8(s) => write!(f, "{}", s),
            Self::U8(n) => write!(f, "{}", n),
            Self::U16(n) => write!(f, "{}", n),
            Self::U32(n) => write!(f, "{}", n),
            Self::U64(n) => write!(f, "{}", n),
            Self::I8(n) => write!(f, "{}", n),
            Self::I16(n) => write!(f, "{}", n),
            Self::I32(n) => write!(f, "{}", n),
            Self::I64(n) => write!(f, "{}", n),
            Self::F32(n) => write!(f, "{}", n),
            Self::F64(n) => write!(f, "{}", n),
            Self::Bytes(v) => write!(f, "{:?}", v),
            Self::Range(a,b) => write!(f, "{}-{}", a, b),
            Self::DateTime(d) => write!(f, "{}", d),
            Self::Duration(d) => write!(f, "{}", d),
        }
    }
}

impl RealValue{
    pub fn gt(&self, right: &RealValue) -> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left > right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Boolean(_) => {return HandResult::Err(());},
            RealValue::Range(_left,_right) => {return HandResult::Err(());},
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value > right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Utf8(_) => {return HandResult::Err(());},
            RealValue::Duration(_) => {return HandResult::Err(());},
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value > right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value > right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value > right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value > right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(_) => {
                return HandResult::Err(());
            }
        }
    }
    pub fn ge(&self, right: &RealValue) -> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left >= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Boolean(_) => {return HandResult::Err(());},
            RealValue::Range(_,_) => {return HandResult::Err(());},
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value >= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Utf8(_) => {return HandResult::Err(());},
            RealValue::Duration(_) => {return HandResult::Err(());},
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value >= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value >= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value >= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value >= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(_) => {return HandResult::Err(());},
        }
    }
    pub fn lt(&self, right: &RealValue) -> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = right {
                    return HandResult::Ok(RealValue::Boolean(left < right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Boolean(_) => {return HandResult::Err(());},
            RealValue::Range(_left,_right) => {return HandResult::Err(());},
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value < right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Utf8(_) => {return HandResult::Err(());},
            RealValue::Duration(_) => {return HandResult::Err(());},
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value < right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value < right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value < right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = right {
                    return HandResult::Ok(RealValue::Boolean(left_value < right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(_) => {return HandResult::Err(());},
        }
    }
    pub fn le(&self, other: &RealValue)-> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::Boolean(_) => {return HandResult::Err(());},
            RealValue::Range(_left,_right) => {return HandResult::Err(());},
            RealValue::Utf8(_) => {return HandResult::Err(());},
            RealValue::Duration(_) => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left <= right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value <= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value <= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value <= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value <= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value <= right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(_) => {return HandResult::Err(());},
        }
    }
    pub fn eq(&self, other: &RealValue)-> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Boolean(left) => {
                if let RealValue::Boolean(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Range(_left,_right) => {return HandResult::Err(());},
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Utf8(left) => {
                if let RealValue::Utf8(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Duration(left) => {
                if let RealValue::Duration(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left == right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(left_value) => {
                if let RealValue::Bytes(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value == right_value));
                }else{
                    return HandResult::Err(());
                }
            },
        }
    }
    pub fn neq(&self, other: &RealValue)-> HandResult {
        match self{
            RealValue::None => {return HandResult::Err(());},
            RealValue::U8(left) => {
                if let RealValue::U8(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U16(left) => {
                if let RealValue::U16(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U32(left) => {
                if let RealValue::U32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::U64(left) => {
                if let RealValue::U64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F32(left) => {
                if let RealValue::F32(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::F64(left) => {
                if let RealValue::F64(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Boolean(left) => {
                if let RealValue::Boolean(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Range(_,_) => {return HandResult::Err(());},
            RealValue::DateTime(left_value) => {
                if let RealValue::DateTime(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Utf8(left) => {
                if let RealValue::Utf8(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Duration(left) => {
                if let RealValue::Duration(right) = other{
                    return HandResult::Ok(RealValue::Boolean(left != right));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I8(left_value) => {
                if let RealValue::I8(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I16(left_value) => {
                if let RealValue::I16(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I32(left_value) => {
                if let RealValue::I32(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::I64(left_value) => {
                if let RealValue::I64(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
            RealValue::Bytes(left_value) => {
                if let RealValue::Bytes(right_value) = other{
                    return HandResult::Ok(RealValue::Boolean(left_value != right_value));
                }else{
                    return HandResult::Err(());
                }
            },
        }
    }
}

impl ElementValue{
    pub fn new(value: Vec<String>)->Self{
        Self{
            element: value,
        }
    }
}

impl RealConfValue{
    pub fn up(self)->ElementRealConfValue{
        match self{
            RealConfValue::RealValue(v) => {
                ElementRealConfValue::RealValue(v)
            },
            RealConfValue::ConfValue(v) => {
                ElementRealConfValue::ConfValue(v)
            },
        }
    }
}