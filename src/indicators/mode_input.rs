use crate::util::AsyncRecvData;
use tokio::sync::RwLock;
use std::collections::HashMap;
use futures::executor::block_on;

lazy_static!{
    pub static ref MODE_WORKSPACE_INPUT: RwLock<HashMap<String, AsyncRecvData>> = {
        RwLock::new(HashMap::new())
    };
    pub static ref MODE_WORKNODE_INPUT: RwLock<HashMap<String, AsyncRecvData>> = {
        RwLock::new(HashMap::new())
    };
    pub static ref MODE_SINK_INPUT: RwLock<HashMap<String, AsyncRecvData>> = {
        RwLock::new(HashMap::new())
    };
}

pub async fn work_space_input_register(name: &str, input: AsyncRecvData){
    let mut mode_workspace_input = MODE_WORKSPACE_INPUT.write().await;
    mode_workspace_input.insert(name.to_string(), input);
}

pub async fn work_node_input_register(name: &str, input: AsyncRecvData){
    let mut mode_worknode_input = MODE_WORKNODE_INPUT.write().await;
    mode_worknode_input.insert(name.to_string(), input);
}

pub async fn sink_input_register(name: &str, input: AsyncRecvData){
    let mut mode_sink_input = MODE_SINK_INPUT.write().await;
    mode_sink_input.insert(name.to_string(), input);
}

pub fn restful_channel_input()->String{
    let mut tmp = HashMap::new();

    let mut tmp_workspace = HashMap::new();
    let work_space = block_on(MODE_WORKSPACE_INPUT.write());
    for (name,input) in work_space.iter(){
        tmp_workspace.insert(name.to_string(), block_on(input.len()));
    }
    tmp.insert("work_space".to_string(), tmp_workspace);

    let mut tmp_worknode = HashMap::new();
    let work_node = block_on(MODE_WORKNODE_INPUT.write());
    for (name,input) in work_node.iter(){
        tmp_worknode.insert(name.to_string(), block_on(input.len()));
    }
    tmp.insert("work_node".to_string(), tmp_worknode);

    let mut tmp_sink = HashMap::new();
    let sink = block_on(MODE_SINK_INPUT.write());
    for (name,input) in sink.iter(){
        tmp_sink.insert(name.to_string(), block_on(input.len()));
    }
    tmp.insert("sink".to_string(), tmp_sink);

    match serde_json::to_string_pretty(&tmp){
        Ok(t) => {t},
        Err(e) => {
            format!("serde_json err {:?}", e)
        },
    }
}


