use sapeur::{Parser, parse_text, ParserResult};
use crate::data_frame::value::ElementValue;
use sapeur::text::{TextChar, text_key_world, text_just};
use crate::conf_init::{MapResult, MapErr};

pub fn parse_element_value<'a>()
    ->impl Parser<char, ElementValue, TextChar>+'a {
    let parse_just_element = text_key_world()
        .separated_by(text_just(",").expand_by_repeated(text_just(" ")), false)
        .delimited_by(text_just("(").expand_by_repeated(text_just(" ")), text_just(")").expand_by_repeated(text_just(" ")))
        .map_end(|o|
           ElementValue::new(o)
        );
    parse_just_element
}

pub fn conf_parse_element_value(desc: &str) ->MapResult<ElementValue>{
    info!("conf_parse_logical_expr_func [{}]", desc);

    let res = parse_text(desc,
                         &parse_element_value(),
                         vec!["no"].as_slice());
    match res {
        ParserResult::OkEnd(t) => {
            return MapResult::Ok(t);
        },
        ParserResult::Ok(_) => {
            warn!("conf_parse_element_value failed: not to OkEnd");
            return MapResult::Err(MapErr::new(format!("[{}] parse Ok not to OkEnd", desc)));
        },
        ParserResult::Err(e) => {
            warn!("conf_parse_element_value failed: at offset {}", e.offset);
            return MapResult::Err(MapErr::new(format!("[{}] Err at offset [{}]",
                                                      desc, e.offset)))
        },
        ParserResult::ErrEnd(e) => {
            warn!("conf_parse_element_value failed: at offset {}", e.offset);
            return MapResult::Err(MapErr::new(format!("[{}] ErrEnd at offset [{}]",
                                                      desc, e.offset)))
        },
    }
}