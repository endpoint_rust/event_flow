
use crate::logical_plan::expr::window::{TimeUnit};

#[derive(Deserialize,Debug)]
pub struct FuncConf{
    pub name: String,
    pub condition: Option<String>,
    pub desc: String,
}
#[derive(Deserialize,Debug)]
pub struct PreDefine{
    pub name: String,
    pub desc: String,
}
#[derive(Deserialize,Debug)]
pub struct FilterConf{
    pub name: String,
    pub desc: String,
}
#[derive(Deserialize,Debug)]
pub struct WorkNodeConf{
    pub name: String,
    pub enable: bool,
    pub input: Vec<String>,
    pub pre_define: Option<Vec<PreDefine>>,//通用处理预定义
    pub filter: Option<FilterConf>,//过滤类函数配置
    pub func: Option<Vec<FuncConf>>,//通用处理类函数配置
    pub window: Option<WindowConf>, //窗口处理配置
}

#[derive(Deserialize,Debug)]
pub struct WindowConf{
    pub name: String,
    //window partition_by 的几种灵活配置场景，支持对多元素处理的基础上进行组合partition
    // (a,b,c) (d,e,f) element_func((h,i,j),5)
    pub partition_by: Option<(bool,String)>,
    pub accumulator: String,
    pub sequential: String,
    pub uniqueness: String,
    pub window_time: u64,
    pub slide_time: u64,
    pub wait_time: u64,
    pub time_unit: TimeUnit,
}