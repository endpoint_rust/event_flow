
use crate::run_plan::func::{RunPlanExprFuncCombWithName};

pub struct FilterInstance{
    pub func: Box<RunPlanExprFuncCombWithName>,
}

impl FilterInstance{
    pub fn new(func: Box<RunPlanExprFuncCombWithName>)->Box<FilterInstance>{
        return Box::new(FilterInstance{
            func: func,
        });
    }
}