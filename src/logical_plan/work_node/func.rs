
use crate::run_plan::func::{RunPlanExprFunc, RunPlanExprFuncCombWithName};

pub struct FuncInstance{
    pub condition: Option<Box<RunPlanExprFuncCombWithName>>,
    pub func: Box<RunPlanExprFunc>,
}

impl FuncInstance{
    pub fn new(condition: Option<Box<RunPlanExprFuncCombWithName>>, func: Box<RunPlanExprFunc>)
        ->Box<FuncInstance>{

        return Box::new(FuncInstance{
            condition: condition,
            func: func,
        });
    }
}