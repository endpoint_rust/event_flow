
use crate::run_plan::window::RunPlanExprWindow;

pub struct WindowInstance{
    pub window: Box<RunPlanExprWindow>,
}

impl WindowInstance{
    pub fn new(window: Box<RunPlanExprWindow>)->Box<WindowInstance>{
        Box::new(
            WindowInstance{
                window: window,
            }
        )
    }
}