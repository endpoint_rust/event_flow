#![allow(dead_code)]
#![feature(try_trait_v2)]
#![feature(associated_type_defaults)]
#![feature(hasher_prefixfree_extras)]
#![feature(let_chains)]
#![allow(unused_variables)]
#![feature(ip)]
#![feature(async_closure)]
#![feature(array_chunks)]

extern crate log;
extern crate log4rs;
extern crate chrono;
extern crate async_channel;
extern crate once_cell;
extern crate async_trait;
extern crate clap;
extern crate tokio;
extern crate toml;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;
extern crate idata;
extern crate snowflake;
extern crate serde_yaml;
extern crate async_recursion;
extern crate elasticsearch;
extern crate async_std;
#[cfg(feature="source_zmq")]
extern crate zeromq;
extern crate tremor_value;
extern crate ouroboros;
#[cfg(feature="sink_clickhouse")]
extern crate clickhouse_rs;
extern crate rs_es;
extern crate crossbeam_skiplist_piedb;
extern crate simd_json;
extern crate simd_json_derive;
#[macro_use]
extern crate rocket;
extern crate serde_json;
extern crate bytes;
extern crate sapeur;


use crate::conf_init::{conf_map_run, MapResult, EVENT_FLOW_CMD_CONF_INSTANCE};
use crate::restful::restful_run;
use std::time::Duration;
use crate::util::dyn_runtime::dyn_runtime_run;

pub mod test;
pub mod conf_parse;
pub mod conf_init;
pub mod util;
pub mod data_frame;
pub mod logical_plan;
pub mod physical_plan;
pub mod run_plan;
pub mod source;
pub mod sink;
pub mod indicators;
pub mod restful;
pub mod timeout;

fn main() {

    console_subscriber::init();

    let now = std::time::SystemTime::now();

    println!("event_flow run at {:?}", now);

    if let Err(e) = log4rs::init_file(&EVENT_FLOW_CMD_CONF_INSTANCE.log4rs_yaml, Default::default()){
        println!("log4rs init_file failed, {}", e);
        return;
    }

    info!("event_flow run at {:?}", now);

    if let MapResult::Err(e) = conf_map_run(){
        println!("conf_map_run failed {:?}", e);
        return;
    }

    dyn_runtime_run();

    restful_run();

    loop{
        std::thread::sleep(Duration::from_secs(100));
    }
}
