

/*
    主要用于对SingleData里面的Metadata进行相关操作，
    以进行一些事件数据无关的操作，适应更多的业务场景。
    目前来看，没有太通用的metadata能力，先暂时分大类处理吧。

     函数命名规范: meta_type_hand
*/

pub mod sorted_record;
pub mod statistic_record;

