

use crate::physical_plan::PhysicalPlanExprArg;
use crate::data_frame::single_data::SingleData;
use crate::run_plan::HandResult;
use crate::data_frame::value::RealValue;

pub fn meta_sorted_record_append(args: &[Box<PhysicalPlanExprArg>], data: &mut SingleData)
                                 -> HandResult {

    if args.len() != 2{
        return HandResult::Err(());
    }

    let key = &args[0];
    let value = &args[1];

    let key = match key.as_ref(){
        PhysicalPlanExprArg::RealValue(key) => {key.to_string()},
        PhysicalPlanExprArg::ConfValue(key) => {key.value.clone()},
        PhysicalPlanExprArg::ElementValue(key) => {
            let key = data.data.get_key_value(key)?;
            key.to_string()
        },
    };

    let value = match value.as_ref(){
        PhysicalPlanExprArg::RealValue(value) => {value.to_string()},
        PhysicalPlanExprArg::ConfValue(value) => {value.value.clone()},
        PhysicalPlanExprArg::ElementValue(value) => {
            let value = data.data.get_key_value(value)?;
            value.to_string()
        },
    };

    data.metadata.sorted_record.insert(key, value);

    return HandResult::Ok(RealValue::None);
}
