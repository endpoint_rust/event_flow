
#[derive(Serialize)]
pub struct BackInfo{
    state: bool,
    info: String,
}

impl BackInfo{
    pub fn new_err(info: String)->String{
        let tmp = BackInfo{
            state: false,
            info: info,
        };
        match serde_json::to_string(&tmp){
            Ok(t) => {t},
            Err(e) => {
                error!("BackInfo Serialilze err: {:?}", e);
                std::process::exit(-1);
            },
        }
    }
}