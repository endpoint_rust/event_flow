
use crate::sink::{Sink, SinkResult, SinkData};
use crate::conf_init::MapResult;
use tokio::runtime::Runtime;

#[derive(Deserialize,Debug)]
pub struct StdoutConf{
    nomean: Option<String>,
}

#[derive(Debug)]
pub struct StdOut;

impl StdOut{
    pub fn new()->MapResult<Box<dyn Sink>>{
        MapResult::Ok(Box::new(StdOut{}))
    }
}

#[async_trait]
impl Sink for StdOut{
    async fn init(&mut self)->bool{
        true
    }
    async fn store(&mut self, runtime: &Runtime, data: Vec<Vec<SinkData>>, _burst: usize, _concurrent: usize)->SinkResult{

        for data in data.into_iter() {
            for data in data.into_iter() {
                let desc = data.data.serialize();
                println!("{}", desc);
            }
        }

        return SinkResult::Ok(());
    }
}