
use timer::{Timer};
use chrono::Duration;
use std::collections::HashMap;
use crossbeam_channel::{bounded, Receiver, Sender, TrySendError};
use crate::indicators::{indicators_key_ref, Op, IndicatorsMode};
use crate::conf_init::normal_conf;

enum TimerMsg{
    Once(String, Duration, Sender<bool>),
    Repeat(String, Duration, Sender<bool>),
    UnReg(String),
}

unsafe impl Send for TimerMsg {}

lazy_static!{
    static ref TIMER_CHANNEL: (Sender<TimerMsg>,Receiver<TimerMsg>) = {
         let normal_conf = normal_conf();
        bounded(normal_conf.timer_channel_size)
    };
}

pub fn timer_run(){

    match std::thread::Builder::new().name("timer".to_string()).spawn(||{
        let timer = Timer::new();
        let mut guard_list = HashMap::new();

        loop{
            match TIMER_CHANNEL.1.recv(){
                Ok(t) => {
                    match t{
                        TimerMsg::Once(name, d, s) => {
                            if let Some(_) = guard_list.get(name.as_str()){
                                let idicator = format!("{} repeat register", name);
                                indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", idicator.as_str(), 1);
                                if let Err(e) = s.send(false){
                                    error!("timer register send err [{:?}]", e);
                                    std::process::exit(-1);
                                }
                                continue;
                            }

                            if let Err(e) = s.send(true){
                                error!("timer register send err [{:?}]", e);
                                std::process::exit(-1);
                            }

                            let indicator_1 = format!("{} bak channel full", name);
                            let indicator_2 = format!("{} bak channel Discon", name);

                            let guard = timer.schedule_with_delay(d,move ||{

                                if let Err(e) = s.try_send(true){
                                    match e{
                                        TrySendError::Full(_) => {
                                            indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", indicator_1.as_str(), 1);
                                        },
                                        TrySendError::Disconnected(_) => {
                                            indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", indicator_2.as_str(), 1);
                                        },
                                    }
                                }
                            });
                            guard_list.insert(name, guard);
                        },
                        TimerMsg::Repeat(name, d, s) => {
                            if let Some(_) = guard_list.get(name.as_str()){
                                let idicator = format!("{} repeat register", name);
                                indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", idicator.as_str(), 1);
                                if let Err(e) = s.send(false){
                                    error!("timer register send err [{:?}]", e);
                                    std::process::exit(-1);
                                }
                                continue;
                            }

                            if let Err(e) = s.send(true){
                                error!("timer register send err [{:?}]", e);
                                std::process::exit(-1);
                            }

                            let indicator_1 = format!("{} bak channel full", name);
                            let indicator_2 = format!("{} bak channel Discon", name);

                            let guard = timer.schedule_repeating(d, move ||{
                                if let Err(e) = s.try_send(true){
                                    match e{
                                        TrySendError::Full(_) => {
                                            indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", indicator_1.as_str(), 1);
                                        },
                                        TrySendError::Disconnected(_) => {
                                            indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", indicator_2.as_str(), 1);
                                        },
                                    }
                                }
                            });
                            guard_list.insert(name, guard);
                        },
                        TimerMsg::UnReg(name) => {
                            guard_list.remove(name.as_str());
                        },
                    }
                },
                Err(e) => {
                    error!("TIMER_CHANNEL recv err {:?}", e);
                    std::process::exit(-1);
                },
            }
        }
    }){
        Ok(t) => {},
        Err(e) => {
            error!("timer_run build thread err: {:?}", e);
            std::process::exit(-1);
        },
    }
}

/*
为避免逻辑混乱，名称相同的timer不允许重复注册
*/
pub fn timer_reg_repeat(name: String, d: Duration) ->Option<Receiver<bool>>{

    let normal_conf = normal_conf();

    let (s,r) = bounded(normal_conf.timer_channel_size);
    let tmp = TimerMsg::Repeat(name, d, s);

    if let Err(e) = TIMER_CHANNEL.0.try_send(tmp){
        match e{
            TrySendError::Disconnected(_e) => {
                error!("Timer channel Disconnected");
                std::process::exit(-1);
            }
            TrySendError::Full(t) => {
                indicators_key_ref(Op::Add, IndicatorsMode::Channel, "timer", "reg channel full", 1);

                if let Err(e) = TIMER_CHANNEL.0.send(t){
                    error!("Timer channel resend err {:?}", e);
                    std::process::exit(-1);
                }

                match r.recv(){
                    Ok(ro) => {
                        if !ro{
                            return None;
                        }
                    },
                    Err(e) => {
                        error!("Timer register bak err {:?}", e);
                        std::process::exit(-1);
                    },
                }

                return Some(r);
            }
        }
    }else{
        return Some(r);
    }
}
